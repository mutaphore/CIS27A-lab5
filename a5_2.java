/*
@author: Dewei Chen 
@date: 2-12-2012
@class: CIS27A
@instructor: Dave Harden
@filename: a5_2.java
@description: This program asks user to input an integer greater than 0 and 
calculates the sum of the squares of the numbers from 1 to that integer. For 
example, if the integer equals 4, the sum of the squares is 30 (1 + 4 + 9 + 16). 
The program should repeat this process until the user wants to quit. An input 
value less than or equal to 0 signals that the user wants to quit.
*/

import java.util.Scanner;

public class a5_2 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		int num;
		int sum;
		
		//Run loop until number entered is less than 1
		do{
			//Reset the sum to 0 every time the loop is ran
			sum = 0;
			System.out.print("Enter a number greater than 0 (less than 1 to quit): ");
			num = input.nextInt();
			
			for (int i=1 ; i <= num ; i++)
				sum += i*i;
			//Print out the sum only when it is greater than 0, so user is not quitting program
			if (sum > 0)
				System.out.printf("The sum of the squares from 1 to %d is %d\n",num, sum);
		}while(num >= 1);
		
	}

}
