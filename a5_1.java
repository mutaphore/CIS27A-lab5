/*
@author: Dewei Chen 
@date: 2-12-2012
@class: CIS27A
@instructor: Dave Harden
@filename: a5_1.java
@description: This program asks the user how many numbers will be entered 
and then has the user enter those numbers. When this is done, it reports to the 
user the position of the first 7 entered and the last 7 entered. The position
is at which index that the users enters the number 7.
*/

import java.util.Scanner;

public class a5_1 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		int numTotal;
		int currentNum;
		//Define tracker variables that keeps track of position indexes of 7s
		int firstSeven = 0;	
		int lastSeven = 0;
		
		System.out.print("How many numbers will be entered? ");
		numTotal = input.nextInt();
		
		//Run loop until all numbers has been entered
		for (int i=1 ; i <= numTotal ; i++)
		{
			System.out.print("Enter num: ");
			currentNum = input.nextInt();
			
			if (currentNum == 7)
			{
				//In case where it is the first 7, both tracker variables will be 0
				if (firstSeven == 0 && lastSeven == 0)
				{	
					firstSeven = i;
					lastSeven = i;
				}
				//if it is not the first 7, assume it will be the last 7
				else 
					lastSeven = i;
			}
		}
		
		//Print out a different message when no sevens were entered
		if (firstSeven == 0 && lastSeven == 0)
			System.out.println("Sorry, no sevens were entered.");
		else
		{
			System.out.println("The first 7 was in position " + firstSeven);
			System.out.println("The last 7 was in position " + lastSeven);
		}

	}

}
